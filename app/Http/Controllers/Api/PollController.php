<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Poll;
use Illuminate\Support\Facades\DB;


class PollController extends Controller
{

    public function show(Poll $poll)
    {
        $query = Poll::find($poll->id);
        $query->increment('views');

        $json = ["poll_id" => $poll->id,
            "poll_description" => $poll->description,
            "options" => [],
        ];

        foreach($query->options as $option) {
            $json["options"][] = ["option_id" => $option->id,
                "option_description" => $option->description,
            ];
        }

        return response()->json($json);
    }

    public function store(Request $request)
    {
        $poll = Poll::create([
            'description' => $request->get('poll_description'),
        ]);
        foreach($request->get('options') as $option) {
            $options[]['description'] = $option;
        }
        $poll->options()->createMany($options);
        return response()->json(["poll_id" => $poll->id], 201);
    }

    public function vote(Request $request, Poll $poll)
    {
       $query = DB::table('options')
        ->where('id', $request->get('option_id'))
        ->where('poll_id', $poll->id);

        if($query->exists()) {
            $query->increment('quantity');
            return response()->json(["status" => true]);
        } else {
            return response()->json(["message" => "Not Found"], 404);
        }
    }

    public function stats(Poll $poll)
    {
        $query = Poll::find($poll->id);

        $json["views"] = $poll->views;
        foreach($query->options as $option) {
            $json["votes"][] = ["option_id" => $option->id,
                "qty" => $option->quantity,
            ];
        }

        return response()->json($json);
    }
}
