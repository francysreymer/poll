<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->name('api.')->group(function() {
    Route::prefix('poll')->group(function() {

        Route::get('/{poll}', 'PollController@show')->name('show_polls');

        Route::post('/', 'PollController@store')->name('store_polls');

        Route::post('/{poll}/vote', 'PollController@vote')->name('vote_polls');

        Route::get('/{poll}/stats', 'PollController@stats')->name('stats_polls');
    });

});
