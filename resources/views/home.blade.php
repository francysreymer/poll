<html>
    <body>

        <div id="app">
            <poll-component></poll-component>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>